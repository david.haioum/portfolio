<?php

//require all classes
require "src/phpClasses/Competence.php";
require "src/phpClasses/HomeSection.php";
require "src/phpClasses/Profil.php";
require "src/phpClasses/Project.php";
require "src/phpClasses/Skill.php";
require "src/phpClasses/SocialMedia.php";

require "src/phpClassesManagers/Manager.php";
require "src/phpClassesManagers/CompetencesManager.php";
require "src/phpClassesManagers/HomeSectionManager.php";
require "src/phpClassesManagers/ProfilManager.php";
require "src/phpClassesManagers/ProjectsManager.php";
require "src/phpClassesManagers/SkillsManager.php";
require "src/phpClassesManagers/SocialMediasManager.php";

require_once './vendor/autoload.php';

$host = "localhost";
$db = "portfolio";
$login = "root";
$pass = "";

//comptences manager
$cm = new CompetencesManager($host,$db,$login,$pass);
$cm->connect();
//home section manager
$hsm = new HomesectionManager($host,$db,$login,$pass);
$hsm->connect();
//home section
$hs = $hsm->select();
//profil manager
$pm = new ProfilManager($host,$db,$login,$pass);
$pm->connect();
//profil
$p = $pm->select();
//projects manager
$prm = new ProjectsManager($host,$db,$login,$pass);
$prm->connect();
//skills manager
$skm = new SkillsManager($host,$db,$login,$pass);
$skm->connect();
//socialmedia manager
$smm = new SocialMediasManager($host,$db,$login,$pass);
$smm->connect();

//template render
$loader = new Twig_Loader_Filesystem(__DIR__.'/templates');
//environnement with options

/**

don't forget to renable the cache to optimize performances
and throw all content in tmp/ before disabling

*/

//when developing, disable the cache system
//don't forget to enable it at the end
$twig = new Twig_Environment($loader, [
    'cache' => false,//__DIR__.'/tmp',
    'debug' => false //to use dump()
]);

//debug extension
$twig->addExtension(new Twig_Extension_Debug());

echo $twig->render('indexModel.html', [
    "homesection" => [ 
        "title" => $hs->getName(),
        "profession" => $hs->getProfession(),
        "companyWebSite" => $hs->getCompanyWebSite(),
        "companyName" => $hs->getCompanyName(),
    ],
    "skills" => $skm->selectAllFrom("skills"),
    "profil" => [
        "face" => $p->getFaceImgLink(),
        "profession" => $p->getProfession(),
        "companyLink" => $p->getCompanyLink(),
        "companyName" => $p->getCompanyName(),
        "placeLink" => $p->getPlaceLink(),
        "place" => $p->getPlace(),
        "hobbies" => $p->getHobbies()
    ],
    "competences" => $cm->selectAllFromOrder("competences"),
    "projects" => $prm->selectAllFrom("projects"),
    "socialMedias" => $cm->selectAllFrom("socialmedia")

]);


?>