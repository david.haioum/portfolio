<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PORTFOLIO ADMIN PANEL</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="src/css/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="src/js/scripts/execScript.js"></script>
    <script src="src/js/scripts/displayFormData.js"></script>
</head>

<body class="bg-blue">
    <h1 class="text-white text-center margin-top-30px">Portfolio Admin Panel</h1>
    <div class="container-fluid margin-top-30px">
        <div id="accordion">
            <div class="card">
                <div class="card-header">
                    <a class="collapsed card-link bolder" data-toggle="collapse" href="#collapse1">
                        HOME SECTION
                    </a>
                </div>
                <div id="collapse1" class="collapse" data-parent="#accordion">
                    <div class="container-fluid mt-3 mb-3">
                        <form id="homesection_form">
                            <div class="form-group">
                                <label for="hs_name">Full Name :</label>
                                <input type="text" class="form-control" name="hs_name">
                            </div>
                            <div class="form-group">
                                <label for="hs_profession">Profession :</label>
                                <input type="text" class="form-control" name="hs_profession">
                            </div>
                            <div class="form-group">
                                <label for="hs_companyName">Company name :</label>
                                <input type="text" class="form-control" name="hs_companyName">
                            </div>
                            <div class="form-group">
                                <label for="hs_companyWebSite">Company website link :</label>
                                <input type="text" class="form-control" name="hs_companyWebSite">
                            </div>
                            <div class="form-group">
                                <label for="hs_email">Email :</label>
                                <input type="email" class="form-control" name="hs_email">
                            </div>
                        </form>
                        <button onclick="execScript('#homesection_form','pannel/updateHomesection.php')" class="btn bg-turquoise-green w-100 text-white bolder">Update</button>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <a class="collapsed card-link bolder" data-toggle="collapse" href="#collapse2">
                        PROFIL
                    </a>
                </div>
                <div id="collapse2" class="collapse" data-parent="#accordion">
                    <div class="container-fluid mt-3 mb-3">
                        <form id="profil_form">
                            <div class="form-group">
                                <label for="profil_profession">Profession :</label>
                                <input type="text" class="form-control" name="profil_profession" placeholder="profession">
                            </div>
                            <div class="form-group">
                                <label for="profil_place">Place :</label>
                                <input type="text" class="form-control" name="profil_place" placeholder="place">
                            </div>
                            <div class="form-group">
                                <label for="profil_placeLink">place website link :</label>
                                <input type="text" class="form-control" name="profil_placeLink" placeholder="place website link">
                            </div>
                            <div class="form-group">
                                <label for="profil_companyName">Company name :</label>
                                <input type="text" class="form-control" name="profil_companyName" placeholder="company name">
                            </div>
                            <div class="form-group">
                                <label for="profil_companyLink">Company website link :</label>
                                <input type="text" class="form-control" name="profil_companyLink" placeholder="companyLink">
                            </div>
                            <div class="form-group">
                                <label for="profil_faceImgLink">Select Profil image :</label>
                                <select class="form-control" name="profil_faceImgLink">
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="profil_hobbies">Tiny description :</label>
                                <textarea class="form-control" placeholder="description" name="profil_hobbies"></textarea>
                            </div>
                        </form>
                        <button onclick="execScript('#profil_form','pannel/updateProfil.php')" class="btn bg-turquoise-green w-100 text-white bolder">Update</button>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <a class="collapsed card-link bolder" data-toggle="collapse" href="#collapse3">
                        COMPETENCES
                    </a>
                </div>
                <div id="collapse3" class="collapse" data-parent="#accordion">
                    <div class="container-fluid mt-3 mb-3">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#update3">Update</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#add3">Add</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#delete3">Delete</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="update3" class="container-fluid tab-pane active"><br>
                                <form id="update_competence_form">
                                    <div class="form-group">
                                        <label for="competence_name">Select a competence to update :</label>
                                        <select onchange="getDataFromSelect('pannel/select/selectCompetenceByName.php',displayCompetencesUpdateData,'competence_name');" name="competence_name" class="form-control"></select>
                                    </div>
                                    <div class="form-group">
                                        <label for="competence_newName">Competence name :</label>
                                        <input type="text" class="form-control" name="competence_newName">
                                    </div>
                                    <div class="form-group">
                                        <label for="competence_percentage">Percentage of the skill (in %) :</label>
                                        <input type="number" min="0" max="100" class="form-control" name="competence_percentage">
                                    </div>
                                </form>
                                <button onclick="execScript('#update_competence_form','pannel/updateCompetence.php'); getData('view/selectAllFromCompetences',displayCompetencesFormData);
                                "  class="btn bg-turquoise-green w-100 text-white bolder">Update</button>
                            </div>
                            <div id="add3" class="container-fluid tab-pane"><br>
                                <form id="add_competence_form">
                                    <div class="form-group">
                                        <label for="competence_add_name">Competence name :</label>
                                        <input type="text" class="form-control" name="competence_add_name" placeholder="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="competence_add_percentage">Percentage of the skill (in %) :</label>
                                        <input type="number" min="0" max="100" class="form-control" name="competence_add_percentage" placeholder="100">
                                    </div>
                                </form>
                                <button onclick="execScript('#add_competence_form','pannel/addCompetence.php');getData('view/selectAllFromCompetences',displayCompetencesFormData);"  class="btn bg-turquoise-green w-100 text-white bolder">Add</button>
                            </div>
                            <div id="delete3" class="container-fluid tab-pane"><br>
                                <form id="delete_competence_form">
                                    <div class="form-group">
                                        <label for="competence_delete_name">Select a competence to delete :</label>
                                        <select name="competence_delete_name" class="form-control">
                                        </select>
                                    </div>
                                </form>
                                <button onclick="execScript('#delete_competence_form','pannel/deleteCompetence.php');getData('view/selectAllFromCompetences',displayCompetencesFormData);"  class="btn bg-turquoise-green w-100 text-white bolder">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <a class="collapsed card-link bolder" data-toggle="collapse" href="#collapse4">
                        PROJECTS
                    </a>
                </div>
                <div id="collapse4" class="collapse" data-parent="#accordion">
                    <div class="container-fluid mt-3 mb-3">
                        <div class="container-fluid mt-3 mb-3">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#update4">Update</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#add4">Add</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#delete4">Delete</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="update4" class="container-fluid tab-pane active"><br>
                                    <form id="update_project_form">
                                        <div class="form-group">
                                            <label for="project_name">Select a project to update :</label>
                                            <select onchange="getDataFromSelect('pannel/select/selectProjectByName.php',displayProjectsUpdateData,'project_name');" name="project_name" class="form-control">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="project_newName">Project name :</label>
                                            <input type="text" class="form-control" name="project_newName">
                                        </div>
                                        <div class="form-group">
                                            <label for="project_description">Tiny description :</label>
                                            <textarea class="form-control" placeholder="description" name="project_description"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="project_technoName">Technology name :</label>
                                            <input type="text" class="form-control" name="project_technoName">
                                        </div>
                                        <div class="form-group">
                                            <label for="project_imageLink1">Main image link :</label>
                                            <select class="form-control" name="project_imageLink1">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="project_imageLink2">Second image link :</label>
                                            <select class="form-control" name="project_imageLink2">
                                            </select>                                        
                                        </div>
                                    </form>
                                    <button onclick="execScript('#update_project_form','pannel/updateProject.php'); getData('view/selectAllFromProjects',displayProjectFormData);
                                    "  class="btn bg-turquoise-green w-100 text-white bolder">Update</button>
                                </div>
                                <div id="add4" class="container-fluid tab-pane"><br>
                                    <form id="add_project_form">
                                        <div class="form-group">
                                            <label for="project_add_newName">Project name :</label>
                                            <input type="text" class="form-control" name="project_add_newName"
                                                placeholder="new name">
                                        </div>
                                        <div class="form-group">
                                            <label for="project_add_description">Tiny description :</label>
                                            <textarea class="form-control" placeholder="description" name="project_add_description"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="project_add_technoName">Technology name :</label>
                                            <input type="text" class="form-control" name="project_add_technoName"
                                                placeholder="techno name">
                                        </div>
                                        <div class="form-group">
                                            <label for="project_add_imageLink1">Main image link :</label>
                                            <select class="form-control" name="project_add_imageLink1">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="project_add_imageLink2">Second image link :</label>
                                            <select class="form-control" name="project_add_imageLink2">
                                            </select>
                                        </div>
                                    </form>
                                    <button onclick="execScript('#add_project_form','pannel/addProject.php');getData('view/selectAllFromProjects',displayProjectFormData);"  class="btn bg-turquoise-green w-100 text-white bolder">Add</button>
                                </div>
                                <div id="delete4" class="container-fluid tab-pane"><br>
                                    <form id="delete_project_form">
                                        <div class="form-group">
                                            <label for="project_delete_name">Select a project to delete :</label>
                                            <select name="project_delete_name" class="form-control"></select>
                                        </div>
                                    </form>
                                    <button onclick="execScript('#delete_project_form','pannel/deleteProject.php');getData('view/selectAllFromProjects',displayProjectFormData);"  class="btn bg-turquoise-green w-100 text-white bolder">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <a class="collapsed card-link bolder" data-toggle="collapse" href="#collapse5">
                        SKILLS
                    </a>
                </div>
                <div id="collapse5" class="collapse" data-parent="#accordion">
                    <div class="container-fluid mt-3 mb-3">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#update6">Update</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#add6">Add</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#delete6">Delete</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="update6" class="container-fluid tab-pane active"><br>
                                <form id="update_skill_form">
                                    <div class="form-group">
                                        <label for="skill_name">Select a skill to update :</label>
                                        <select onchange="getDataFromSelect('pannel/select/selectSkillByName.php',displaySkillsUpdateData,'skill_name');" name="skill_name" class="form-control">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="skill_newName">skill name :</label>
                                        <input type="text" class="form-control" name="skill_newName">
                                    </div>
                                    <div class="form-group">
                                        <label for="skill_newLogoLink">skill logo link :</label>
                                        <select class="form-control" name="skill_newLogoLink">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="skill_content">Your citation (phrase) :</label>
                                        <textarea class="form-control" placeholder="content" name="skill_content"></textarea>
                                    </div>
                                </form>
                                <button onclick="execScript('#update_skill_form','pannel/updateSkill.php'); getData('view/selectAllFromSkills',displaySkillsFormData);
                                    "  class="btn bg-turquoise-green w-100 text-white bolder">Update</button>
                            </div>
                            <div id="add6" class="container-fluid tab-pane"><br>
                                <form id="add_skill_form">
                                    <div class="form-group">
                                        <label for="skill_add_newName">skill name :</label>
                                        <input type="text" class="form-control" name="skill_add_newName" placeholder="new name">
                                    </div>
                                    <div class="form-group">
                                        <label for="skill_add_newLogoLink">skill logo link :</label>
                                        <select name="skill_add_newLogoLink" class="form-control">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="skill_add_content">Your citation (phrase) :</label>
                                        <textarea class="form-control" placeholder="content" name="skill_add_content"></textarea>
                                    </div>
                                </form>
                                <button onclick="execScript('#add_skill_form','pannel/addSkill.php');getData('view/selectAllFromSkills',displaySkillsFormData);"  class="btn bg-turquoise-green w-100 text-white bolder">Add</button>
                            </div>
                            <div id="delete6" class="container-fluid tab-pane"><br>
                                <form id="delete_skill_form">
                                    <div class="form-group">
                                        <label for="skill_delete_name">Select a skill to delete :</label>
                                        <select name="skill_delete_name" class="form-control"></select>
                                    </div>
                                </form>
                                <button onclick="execScript('#delete_skill_form','pannel/deleteSkill.php');getData('view/selectAllFromSkills',displaySkillsFormData);"  class="btn bg-turquoise-green w-100 text-white bolder">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
            <div class="card">
                <div class="card-header">
                <a class="collapsed card-link bolder" data-toggle="collapse" href="#collapse6">
                    SOCIAL MEDIAS
                </a>
                </div>
                <div id="collapse6" class="collapse" data-parent="#accordion">
                    <div class="container-fluid mt-3 mb-3">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#update7">Update</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#add7">Add</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#delete7">Delete</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="update7" class="container-fluid tab-pane active"><br>
                                <form id="update_socialmedia_form">
                                    <div class="form-group">
                                        <label for="socialmedia_name">Select a social media to update :</label>
                                        <select onchange="getDataFromSelect('pannel/select/selectSocialMediaByName.php',displaySocialMediaUpdateData,'socialmedia_name');" name="socialmedia_name" class="form-control">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="socialmedia_newName">Social media name :</label>
                                        <input type="text" class="form-control" name="socialmedia_newName">
                                    </div>
                                    <div class="form-group">
                                        <label for="socialmedia_newLogoLink">Social media logo link :</label>
                                        <select name="socialmedia_newLogoLink" class="form-control">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="socialmedia_websiteLink">Social media website link :</label>
                                        <textarea class="form-control" name="socialmedia_websiteLink"></textarea>
                                    </div>
                                </form>
                                <button onclick="execScript('#update_socialmedia_form','pannel/updateSocialMedia.php'); getData('view/selectAllFromSocialMedia',displaySocialMediaFormData);
                                    "  class="btn bg-turquoise-green w-100 text-white bolder">Update</button>
                            </div>
                            <div id="add7" class="container-fluid tab-pane"><br>
                                <form id="add_socialmedia_form">
                                    <div class="form-group">
                                        <label for="socialmedia_add_newName">Social media name :</label>
                                        <input type="text" class="form-control" name="socialmedia_add_newName" placeholder="new name">
                                    </div>
                                    <div class="form-group">
                                        <label for="socialmedia_add_websiteLink">Social media website link :</label>
                                        <textarea class="form-control" placeholder="content" name="socialmedia_add_websiteLink"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="socialmedia_add_newLogoLink">Social media logo link :</label>
                                        <select name="socialmedia_add_newLogoLink" class="form-control">
                                        </select>
                                    </div>
                                </form>
                                <button onclick="execScript('#add_socialmedia_form','pannel/addSocialMedia.php');getData('view/selectAllFromSocialMedia',displaySocialMediaFormData);"  class="btn bg-turquoise-green w-100 text-white bolder">Add</button>
                            </div>
                            <div id="delete7" class="container-fluid tab-pane"><br>
                                <form id="delete_socialmedia_form">
                                    <div class="form-group">
                                        <label for="socialmedia_delete_name">Select a social media to delete :</label>
                                        <select name="socialmedia_delete_name" class="form-control">
                                        </select>
                                    </div>
                                </form>
                                <button onclick="execScript('#delete_socialmedia_form','pannel/deleteSocialMedia.php');getData('view/selectAllFromSocialMedia',displaySocialMediaFormData);"  class="btn bg-turquoise-green w-100 text-white bolder">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 

            <div class="card">
                <div class="card-header">
                <a class="collapsed card-link bolder" data-toggle="collapse" href="#collapse7">
                    IMAGES
                </a>
                </div>
                <div id="collapse7" class="collapse" data-parent="#accordion">
                    <div class="container-fluid mt-3 mb-3">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#add8">Add</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#delete8">Delete</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="add8" class="container-fluid tab-pane active"><br>
                                <form id="add_image_form" method="post" action="src/phpScripts/tools/fileUpload.php" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="image_add_name">Upload an image :</label>
                                        <input type="file" class="form-control" name="image_add_name" id="image_add_name">
                                    </div>
                                    <button type="submit" class="btn bg-turquoise-green w-100 text-white bolder">Add</button>
                                </form>
                            </div>
                            <div id="delete8" class="container-fluid tab-pane"><br>
                                <form method="post" id="delete_image_form">
                                    <div class="form-group">
                                        <label for="image_delete_name">Select an image to delete :</label>
                                        <select name="image_delete_name" id="image_delete_name" class="form-control">
                                        </select>
                                    </div>
                                </form>
                                <button onclick="deleteImage('image_delete_name','tools/deleteImage.php');" class="btn bg-turquoise-green w-100 text-white bolder">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="card">
                <div class="card-header">
                    <a class="collapsed card-link bolder" data-toggle="collapse" href="#collapse9">
                        CONTACT
                    </a>
                </div>
                <div id="collapse9" class="collapse" data-parent="#accordion">
                    <div class="container-fluid mt-3 mb-3" >
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Message</th>
                            </tr>
                        </thead>
                        <tbody id="contact_container">
                            
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-3">
        <a href="index.php" class="btn bg-turquoise-green w-100 bolder" style="color: white;">BACK TO HOME</a>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
</body>

</html>