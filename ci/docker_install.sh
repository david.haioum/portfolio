#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# We can enable extensions with this simple line. We need the ZIP extension to run for Composer
docker-php-ext-enable zip

# Let's run composer install
composer install