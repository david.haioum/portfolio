<?php 
    /**
     * describe the home section
     */
    final class HomeSection {
        private $_id;
        private $_name;
        private $_profession;
        private $_companyName;
        private $_companyWebSite;
        private $_email;

        public function __construct($data)
        {
            /* call all the setter */
            $this->_hydrate($data[0]);
        }

        /**
         * call all the setter
         * 
         * @param array $data from the database
         */
        private function _hydrate(array $data)
        {
            /* for each attributs in the database */
            foreach($data as $key => $value){
                //get the setter name of the attributs
                $method = 'set'.ucfirst($key);
                //if the setter exist 
                if(method_exists($this,$method)){
                    $this->$method($value);
                }
            }
        }
       
        /**
         * Get the value of _id
         */ 
        public function getId()
        {
            return $this->_id;
        }

        /**
         * Set the value of _id
         */ 
        public function setId($id)
        {
            $this->_id = $id;
        }

        /**
         * Get the value of _Email
         */ 
        public function getEmail()
        {
            return $this->_email;
        }

        /**
         * Set the value of _email
         */ 
        public function setEmail($email)
        {
            $this->_email = $email;
        }

        /**
         * Get the value of _name
         */ 
        public function getName()
        {
            return $this->_name;
        }

        /**
         * Set the value of _name
         */ 
        public function setName($name)
        {
            $this->_name = $name;
        }

        /**
         * Get the value of _profession
         */ 
        public function getProfession()
        {
            return $this->_profession;
        }

        /**
         * Set the value of _profession
         */ 
        public function setProfession($profession)
        {
            $this->_profession = $profession;
        }

        /**
         * Get the value of _companyName
         */ 
        public function getCompanyName()
        {
            return $this->_companyName;
        }

        /**
         * Set the value of _companyName
         */ 
        public function setCompanyName($companyName)
        {
            $this->_companyName = $companyName;
        }

        /**
         * Get the value of _companyWebSite
         */ 
        public function getCompanyWebSite()
        {
            return $this->_companyWebSite;
        }

        /**
         * Set the value of _companyWebSite
         */ 
        public function setCompanyWebSite($companyWebSite)
        {
            $this->_companyWebSite = $companyWebSite;
        }
    }
?>
