<?php 
    /**
     * describe a socialmedia
     */
    final class SocialMedia {
        private $_id;
        private $_Name;
        private $_WebsiteLink;
        private $_LogoLink;

        public function __construct($data)
        {
            /* call all the setter */
            $this->_hydrate($data[0]);
        }

        /**
         * call all the setter
         * 
         * @param array $data from the database
         */
        private function _hydrate(array $data)
        {
            /* for each attributs in the database */
            foreach($data as $key => $value){
                //get the setter name of the attributs
                $method = 'set'.ucfirst($key);
                //if the setter exist 
                if(method_exists($this,$method)){
                    $this->$method($value);
                }
            }
        }
       
        /**
         * Get the value of _id
         */ 
        public function getId()
        {
            return $this->_id;
        }

        /**
         * Set the value of _id
         */ 
        public function setId($id)
        {
            $this->_id = $id;
        }

        /**
         * Get the value of _Name
         */ 
        public function getName()
        {
            return $this->_Name;
        }

        /**
         * Set the value of _Name
         */ 
        public function setName($name)
        {
            $this->_Name = $name;
        }

        /**
         * Get the value of _WebsiteLink
         */ 
        public function getWebsiteLink()
        {
            return $this->_WebsiteLink;
        }

        /**
         * Set the value of _WebsiteLink
         */ 
        public function setWebsiteLink($websiteLink)
        {
            $this->_WebsiteLink = $websiteLink;
        }

        /**
         * Get the value of _LogoLink
         */ 
        public function getLogoLink()
        {
            return $this->_LogoLink;
        }

        /**
         * Set the value of _LogoLink
         */ 
        public function setLogoLink($logoLink)
        {
            $this->_LogoLink = $logoLink;
        }

    }
?>
