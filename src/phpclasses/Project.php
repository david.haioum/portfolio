<?php 
    /**
     * describe a project
     */
    final class Project {
        private $_id;
        private $_name;
        private $_description;
        private $_imageLink1;
        private $_imageLink2;
        private $_technoName;

        public function __construct($data)
        {
            /* call all the setter */
            $this->_hydrate($data[0]);
        }

        /**
         * call all the setter
         * 
         * @param array $data from the database
         */
        private function _hydrate(array $data)
        {
            /* for each attributs in the database */
            foreach($data as $key => $value){
                //get the setter name of the attributs
                $method = 'set'.ucfirst($key);
                //if the setter exist 
                if(method_exists($this,$method)){
                    $this->$method($value);
                }
            }
        }
       
        /**
         * Get the value of _id
         */ 
        public function getId()
        {
            return $this->_id;
        }

        /**
         * Set the value of _id
         */ 
        public function setId($id)
        {
            $this->_id = $id;
        }

        /**
         * Get the value of _name
         */ 
        public function getName()
        {
            return $this->_name;
        }

        /**
         * Set the value of _name
         */ 
        public function setName($name)
        {
            $this->_name = $name;
        }

        /**
         * Get the value of _description
         */ 
        public function getDescription()
        {
            return $this->_description;
        }

        /**
         * Set the value of _description
         */ 
        public function setDescription($description)
        {
            $this->_description = $description;
        }

        /**
         * Get the value of _imageLink1
         */ 
        public function getImageLink1()
        {
            return $this->_imageLink1;
        }

        /**
         * Set the value of _imageLink1
         */ 
        public function setImageLink1($imageLink1)
        {
            $this->_imageLink1 = $imageLink1;
        }

        /**
         * Get the value of _imageLink2
         */ 
        public function getImageLink2()
        {
            return $this->_imageLink2;
        }

        /**
         * Set the value of _imageLink2
         */ 
        public function setImageLink2($imageLink2)
        {
            $this->_imageLink2 = $imageLink2;
        }

        /**
         * Get the value of _technoName
         */ 
        public function getTechnoName()
        {
            return $this->_technoName;
        }

        /**
         * Set the value of _technoName
         */ 
        public function setTechnoName($technoName)
        {
            $this->_technoName = $technoName;
        }
    }
?>
