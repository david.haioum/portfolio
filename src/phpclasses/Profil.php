<?php 
    /**
     * describe my profil
     */
    final class Profil {
        private $_id;
        private $_place;
        private $_placeLink;
        private $_profession;
        private $_companyName;
        private $_companyLink;
        private $_faceImgLink;
        private $_hobbies; /* short sentence */

        public function __construct($data)
        {
            /* call all the setter */
            $this->_hydrate($data[0]);
        }

        /**
         * call all the setter
         * 
         * @param array $data from the database
         */
        private function _hydrate(array $data)
        {
            /* for each attributs in the database */
            foreach($data as $key => $value){
                //get the setter name of the attributs
                $method = 'set'.ucfirst($key);
                //if the setter exist 
                if(method_exists($this,$method)){
                    $this->$method($value);
                }
            }
        }
       
        /**
         * Get the value of _id
         */ 
        public function getId()
        {
            return $this->_id;
        }

        /**
         * Set the value of _id
         */ 
        public function setId($id)
        {
            $this->_id = $id;
        }

        /**
         * Get the value of _place
         */ 
        public function getPlace()
        {
            return $this->_place;
        }

        /**
         * Set the value of _place
         */ 
        public function setPlace($place)
        {
            $this->_place = $place;
        }

        /**
         * Get the value of _placeLink
         */ 
        public function getPlaceLink()
        {
            return $this->_placeLink;
        }

        /**
         * Set the value of _placeLink
         */ 
        public function setPlaceLink($placeLink)
        {
            $this->_placeLink = $placeLink;
        }

        /**
         * Get the value of _profession
         */ 
        public function getProfession()
        {
            return $this->_profession;
        }

        /**
         * Set the value of _profession
         */ 
        public function setProfession($profession)
        {
            $this->_profession = $profession;
        }

        /**
         * Get the value of _companyName
         */ 
        public function getCompanyName()
        {
            return $this->_companyName;
        }

        /**
         * Set the value of _companyName
         */ 
        public function setCompanyName($companyName)
        {
            $this->_companyName = $companyName;
        }

        /**
         * Get the value of _companyLink
         */ 
        public function getCompanyLink()
        {
            return $this->_companyLink;
        }

        /**
         * Set the value of _companyLink
         */ 
        public function setCompanyLink($companyLink)
        {
            $this->_companyLink = $companyLink;
        }

        /**
         * Get the value of _faceImgLink
         */ 
        public function getFaceImgLink()
        {
            return $this->_faceImgLink;
        }

        /**
         * Set the value of _faceImgLink
         */ 
        public function setFaceImgLink($faceImgLink)
        {
            $this->_faceImgLink = $faceImgLink;
        }

        /**
         * Get the value of _hobbies
         */ 
        public function getHobbies()
        {
            return $this->_hobbies;
        }

        /**
         * Set the value of _hobbies
         */ 
        public function setHobbies($hobbies)
        {
            $this->_hobbies = $hobbies;
        }
    }
?>
