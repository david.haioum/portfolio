<?php  

try 
{
    $conn = new PDO("mysql:host=localhost;dbname=portfolio",'root','');
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT * FROM adminusers"); 
    $stmt->execute();
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    $admin = $stmt->fetchAll();

    if( $admin[0]["login"] == $_POST["login"] && 
        $admin[0]["password"] == md5($_POST["password"])
    ){
        header('Location: ../../../adminPanel.php');
    }else{
        header('Location: ../../../login.php?error=1');
    }
}
catch(PDOException $e)
{
echo "Connection failed: " . $e->getMessage();
}




?>