<?php 

/*
$_POST["skill_add_newName"]
$_POST["skill_add_newLogoLink"]
$_POST["skill_add_content"]
*/

require "../../phpClasses/Skill.php";
require "../../phpClassesManagers/Manager.php";
require "../../phpClassesManagers/SkillsManager.php";

$manager = new SkillsManager("localhost","portfolio","root","");
$manager->connect();

if( isset($_POST["skill_add_newName"]) &&
    isset($_POST["skill_add_newLogoLink"]) &&
    isset($_POST["skill_add_content"])  )
{
    $manager->add(
        $_POST["skill_add_newLogoLink"],
        $_POST["skill_add_newName"],
        $_POST["skill_add_content"]
    );
}

?>