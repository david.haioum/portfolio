<?php 

require "../../../phpClasses/Project.php";
require "../../../phpClassesManagers/Manager.php";
require "../../../phpClassesManagers/ProjectsManager.php";

$manager = new ProjectsManager("localhost","portfolio","root","");
$manager->connect();

$m = $manager->selectByName($_POST["data"]);

$r = array(
    $m->getName(),
    $m->getDescription(),
    $m->getTechnoName(),
    $m->getImageLink1(),
    $m->getImageLink2()
);
echo json_encode($r);

?>