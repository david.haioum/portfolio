<?php 

require "../../../phpClasses/SocialMedia.php";
require "../../../phpClassesManagers/Manager.php";
require "../../../phpClassesManagers/SocialMediasManager.php";

$manager = new SocialMediasManager("localhost","portfolio","root","");
$manager->connect();

$m = $manager->selectByName($_POST["data"]);

$r = array(
    $m->getName(),
    $m->getWebsiteLink(),
    $m->getLogoLink()
);
echo json_encode($r);

?>