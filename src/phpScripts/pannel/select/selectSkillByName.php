<?php 

require "../../../phpClasses/Skill.php";
require "../../../phpClassesManagers/Manager.php";
require "../../../phpClassesManagers/SkillsManager.php";

$manager = new SkillsManager("localhost","portfolio","root","");
$manager->connect();

$m = $manager->selectByName($_POST["data"]);

$r = array(
    $m->getLogoLink(),
    $m->getName(),
    $m->getContent()
);
echo json_encode($r);

?>