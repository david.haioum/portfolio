<?php 

/*
$_POST["profil_profession"]
$_POST["profil_place"]
$_POST["profil_placeLink"]
$_POST["profil_companyName"]
$_POST["profil_companyLink"]
$_POST["profil_faceImgLink"]
$_POST["profil_hobbies"]
*/

require "../../phpClasses/Profil.php";
require "../../phpClassesManagers/Manager.php";
require "../../phpClassesManagers/ProfilManager.php";

$manager = new ProfilManager("localhost","portfolio","root","");
$manager->connect();

$hm = $manager->select();

if(isset($_POST["profil_profession"])){
    $hm->setProfession($_POST["profil_profession"]);
}
if(isset($_POST["profil_place"])){
    $hm->setPlace($_POST["profil_place"]);
}
if(isset($_POST["profil_placeLink"])){
    $hm->setPlaceLink($_POST["profil_placeLink"]);
}
if(isset($_POST["profil_companyName"])){
    $hm->setCompanyName($_POST["profil_companyName"]);
}
if(isset($_POST["profil_companyLink"])){
    $hm->setCompanyLink($_POST["profil_companyLink"]);
}
if(isset($_POST["profil_faceImgLink"])){
    $hm->setFaceImgLink("img/".$_POST["profil_faceImgLink"]);
}
if(isset($_POST["profil_hobbies"])){
    $hm->setHobbies($_POST["profil_hobbies"]);
}

$manager->update($hm);

?>