<?php 

/*
$_POST["project_name"]
$_POST["project_newName"]
$_POST["project_description"]
$_POST["project_technoName"]
$_POST["project_imageLink1"]
$_POST["project_imageLink2"]
*/

require "../../phpClasses/Project.php";
require "../../phpClassesManagers/Manager.php";
require "../../phpClassesManagers/ProjectsManager.php";

$manager = new ProjectsManager("localhost","portfolio","root","");
$manager->connect();

if(isset($_POST["project_name"])){
    $hm = $manager->selectByName($_POST["project_name"]);

    if(isset($_POST["project_newName"])){
        $hm->setName($_POST["project_newName"]);
    }
    if(isset($_POST["project_description"])){
        $hm->setDescription($_POST["project_description"]);
    }
    if(isset($_POST["project_technoName"])){
        $hm->setTechnoName($_POST["project_technoName"]);
    }
    if(isset($_POST["project_imageLink1"])){
        $hm->setImageLink1($_POST["project_imageLink1"]);
    }
    if(isset($_POST["project_imageLink2"])){
        $hm->setImageLink2($_POST["project_imageLink2"]);
    }
    $manager->update($hm);
}



?>