<?php 

/*
$_POST["project_add_newName"]
$_POST["project_add_description"]
$_POST["project_add_technoName"]
$_POST["project_add_imageLink1"]
$_POST["project_add_imageLink2"]
*/

require "../../phpClasses/Project.php";
require "../../phpClassesManagers/Manager.php";
require "../../phpClassesManagers/ProjectsManager.php";

$manager = new ProjectsManager("localhost","portfolio","root","");
$manager->connect();

if( isset($_POST["project_add_newName"]) &&
    isset($_POST["project_add_description"]) &&
    isset($_POST["project_add_technoName"]) &&
    isset($_POST["project_add_imageLink1"]) &&
    isset($_POST["project_add_imageLink2"]) )
{
    $manager->add(
        $_POST["project_add_newName"],
        $_POST["project_add_description"],
        $_POST["project_add_technoName"],
        $_POST["project_add_imageLink1"],
        $_POST["project_add_imageLink2"]
    );
}
echo isset($_POST["project_add_description"]);
?>