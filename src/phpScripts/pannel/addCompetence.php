<?php 

/*
$_POST["competence_add_name"]
$_POST["competence_add_percentage"]
*/

require "../../phpClasses/Competence.php";
require "../../phpClassesManagers/Manager.php";
require "../../phpClassesManagers/CompetencesManager.php";

$manager = new CompetencesManager("localhost","portfolio","root","");
$manager->connect();

if(isset($_POST["competence_add_name"]) && isset($_POST["competence_add_percentage"]) )
{
    $manager->add($_POST["competence_add_name"],$_POST["competence_add_percentage"]);
}

?>