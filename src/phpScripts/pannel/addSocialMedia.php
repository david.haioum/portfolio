<?php 

/*
$_POST["socialmedia_add_newName"]
$_POST["socialmedia_add_newLogoLink"]
$_POST["socialmedia_add_websiteLink"]
*/

require "../../phpClasses/SocialMedia.php";
require "../../phpClassesManagers/Manager.php";
require "../../phpClassesManagers/SocialMediasManager.php";

$manager = new SocialMediasManager("localhost","portfolio","root","");
$manager->connect();

if( isset($_POST["socialmedia_add_newName"]) &&
    isset($_POST["socialmedia_add_newLogoLink"]) &&
    isset($_POST["socialmedia_add_websiteLink"]) )
{

    $manager->add($_POST["socialmedia_add_newName"],$_POST["socialmedia_add_websiteLink"],$_POST["socialmedia_add_newLogoLink"]);
}

?>