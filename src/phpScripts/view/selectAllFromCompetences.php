<?php

/**

this script return as a json , all the content from the table competences

 */

require "../../phpClasses/Competence.php";
require "../../phpClassesManagers/Manager.php";
require "../../phpClassesManagers/CompetencesManager.php";

$manager = new CompetencesManager("localhost","portfolio","root","");
$manager->connect();
$result = $manager->selectAllFrom("competences");

echo json_encode($result);

?>