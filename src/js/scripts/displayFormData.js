/**
 * do an ajax request from a script
 * ad send the response to a callback function
 * 
 * @param {string} script_name 
 * @param {function} callback 
 */
function getData(script_name,callback){
    //console.log(script_name)
    let request;
    //on ready   
    $(document).ready(()=>{
            //console.log("start")
            // Abort any pending request
            if (request) {
                request.abort();
            }
            // Fire off the request to /form.php
            request = $.ajax({
                url: "/src/phpScripts/"+script_name,
                type: "post",
                dataType: "json"
            });

            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR){
                // Log a message to the console
                //console.log(response);
                callback(response);
            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown){
                // Log the error to the console
                console.error(
                    "error :"+
                    textStatus, errorThrown
                );
            });

        });
}

/**
 * request the data from the server from a name in a select
 * 
 * @param {string} script_name 
 * @param {function} callback 
 * @param {string} selectName 
 */
function getDataFromSelect(script_name,callback,selectName){
    //console.log($('select[name="' + selectName + '"]').val());
    let data = $('select[name="' + selectName + '"]').val();
    let request;
    //on ready   
    if( data !== null )
    {
        $(document).ready(()=>{
            //console.log("start")
            // Abort any pending request
            if (request) {
                request.abort();
            }
            // Fire off the request to /form.php
            request = $.ajax({
                url: "/src/phpScripts/"+script_name,
                type: "post",
                data: {"data":data},
                dataType: "json"
            });

            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR){
                // Log a message to the console
                //console.log(response);
                callback(response);
            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown){
                // Log the error to the console
                console.log(
                    ""+
                    textStatus, errorThrown
                );
            });

        });
    }
}

/**
 * add option for a select 
 * 
 * ex : foo
 * result : <option value="foo">bar</option>
 * 
 * @param {string} selectName 
 * @param {mixed} value  
 */
function addSelectOptionForImages(selectName,value)
{
    let name = value.substring(value.lastIndexOf('/')+1);
    $('select[name=\'' + selectName + '\']').append($('<option>', {value:name, text:name}));
}

/**
 * add option for a select for image names only
 * 
 * ex : img/petrie.png
 * result : <option value="petrie.png">petrie.png</option>
 * 
 * @param {string} selectName 
 * @param {array} data 
 * @param {string} exception 
 */
function createImagesSelectFrom(selectName,data,exception)
{
    let name = exception.substring(exception.lastIndexOf('/')+1);
    data.forEach(element => {
        if( element !== name && element !== "." && element !== "..")
        {
            addSelectOptionForImages(selectName,element);
        }
    });
}

/**
 * display data in competence form
 * 
 * @param {string} data 
 */
function displayCompetencesUpdateData(data)
{
    updateInputValue("competence_newName",data[0]);
    updateInputValue("competence_percentage",data[1]);
}

/**
 * display data in projects form
 * 
 * @param {array} data 
 */
function displayProjectsUpdateData(data)
{
    updateInputValue("project_newName",data[0]);
    updateTextArea("project_description",data[1]);
    updateInputValue("project_technoName",data[2]);
    addSelectOptionForImages("project_imageLink1",data[3]);
    addSelectOptionForImages("project_imageLink2",data[4]);
    fetchDataForSelect("tools/selectImageNames","project_imageLink1",data[3],createImagesSelectFrom);
    fetchDataForSelect("tools/selectImageNames","project_imageLink2",data[4],createImagesSelectFrom);
}

/**
 * display data in skills form
 * 
 * @param {array} data 
 */
function displaySkillsUpdateData(data)
{
    updateInputValue("skill_newName",data[1]);
    updateTextArea("skill_content",data[2]);
    addSelectOptionForImages("skill_newLogoLink",data[0]);
    fetchDataForSelect("tools/selectImageNames","skill_newLogoLink",data[0],createImagesSelectFrom);
}

/**
 * display data in socialmedia form
 * 
 * @param {array} data 
 */
function displaySocialMediaUpdateData(data)
{
    //console.log(data);
    updateInputValue("socialmedia_newName",data[0]);
    updateTextArea("socialmedia_websiteLink",data[1]);
    addSelectOptionForImages("socialmedia_newLogoLink",data[2]);
    fetchDataForSelect("tools/selectImageNames","socialmedia_newLogoLink",data[2],createImagesSelectFrom);

}

/**
 * request data to add in a select
 * 
 * @param {string} script 
 * @param {string} selectName 
 * @param {string} exception value to exclude
 * @param {function} callback callback function
 */
function fetchDataForSelect(script,selectName,exception,callback)
{
    let request;
    //on ready  

        $(document).ready(()=>{
            //console.log("start")
            // Abort any pending request
            if (request) {
                request.abort();
            }
            // Fire off the request to /form.php
            request = $.ajax({
                url: "/src/phpScripts/"+script,
                type: "post",
                dataType: "json"
            });

            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR){
                // Log a message to the console
                //console.log(response);
                callback(selectName,response,exception);
                getDataFromSelect('pannel/select/selectCompetenceByName.php',displayCompetencesUpdateData,'competence_name');    
            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown){
                // Log the error to the console
                console.error(
                    "error :"+
                    textStatus, errorThrown
                );
            });

        });

}

/**
 * update a text input
 * 
 * @param {string} name 
 * @param {string} value 
 */
function updateInputValue(name,value)
{
    $("input[name='"+name+"']").val(value);
}

/**
 * update a textarea input
 * 
 * @param {string} name 
 * @param {string} value 
 */
function updateTextArea(name,value)
{
    $("textarea[name='"+name+"']").val(value);
}

/**
 * add 1 option in a select
 * 
 * @param {string} selectName 
 * @param {mixed} value 
 */
function addSelectOption(selectName,value)
{
    $('select[name=\'' + selectName + '\']').append($('<option>', {value:value, text:value}));
}

/**
 * add option from response data (ajax request)
 * 
 * @param {string} selectName 
 * @param {array} data 
 */
function createSelectFrom(selectName,data)
{
    $('select[name=\'' + selectName + '\']').children().remove().end();
    if( $('select[name=\'' + selectName + '\']').children().length === 0 )
    {
        data.forEach(element => {
            addSelectOption(selectName,element[0]);
        });
    }
}

/**
 * from the response data of an ajax request,
 * fill the forms according to the category 
 * 
 * @param {array} data 
 */
function displayHomesectionFormData(data)
{
    updateInputValue("hs_name",data[0][1]);
    updateInputValue("hs_profession",data[0][2]);
    updateInputValue("hs_companyName",data[0][3]);
    updateInputValue("hs_companyWebSite",data[0][4]);
    updateInputValue("hs_email",data[0][5]);
}

/**
 * from the response data of an ajax request,
 * fill the forms according to the category 
 * 
 * @param {array} data 
 */
function displayProfilFormData(data)
{
    //update input values
    updateInputValue("profil_profession",data[0][1]);
    updateInputValue("profil_place",data[0][2]);
    updateInputValue("profil_placeLink",data[0][3]);
    updateInputValue("profil_companyName",data[0][4]);
    updateInputValue("profil_companyLink",data[0][5]);
    //update the select
    fetchDataForSelect("tools/selectImageNames","profil_faceImgLink",data[0][6],createImagesSelectFrom);
    addSelectOptionForImages("profil_faceImgLink",data[0][6])
    //update textarea
    updateTextArea("profil_hobbies",data[0][7]);
}


/**
 * from the response data of an ajax request,
 * fill the forms according to the category 
 * 
 * @param {array} data 
 */
function displayCompetencesFormData(data)
{
    //update
    fetchDataForSelect("pannel/select/selectCompetenceNames","competence_name","",createSelectFrom);
    //delete
    fetchDataForSelect("pannel/select/selectCompetenceNames","competence_delete_name","",createSelectFrom);
}

/**
 * from the response data of an ajax request,
 * fill the forms according to the category 
 * 
 * @param {array} data 
 */
function displayProjectFormData(data)
{
    //update
    fetchDataForSelect("pannel/select/selectProjectNames","project_name","",createSelectFrom);
    //add
    fetchDataForSelect("tools/selectImageNames","project_add_imageLink1","",createImagesSelectFrom);
    fetchDataForSelect("tools/selectImageNames","project_add_imageLink2","",createImagesSelectFrom);
    //delete
    fetchDataForSelect("pannel/select/selectProjectNames","project_delete_name","",createSelectFrom);
}

/**
 * from the response data of an ajax request,
 * fill the forms according to the category 
 * 
 * @param {array} data 
 */
function displaySkillsFormData(data)
{
    //update
    fetchDataForSelect("pannel/select/selectSkillNames","skill_name","",createSelectFrom);
    //add
    fetchDataForSelect("tools/selectImageNames","skill_add_newLogoLink","",createImagesSelectFrom);
    //delete
    fetchDataForSelect("pannel/select/selectSkillNames","skill_delete_name","",createSelectFrom);
}

/**
 * from the response data of an ajax request,
 * fill the forms according to the category 
 * 
 * @param {array} data 
 */
function displaySocialMediaFormData(data)
{
    //update
    fetchDataForSelect("pannel/select/selectSocialMediaNames","socialmedia_name","",createSelectFrom);
    //add
    fetchDataForSelect("tools/selectImageNames","socialmedia_add_newLogoLink","",createImagesSelectFrom);
    //delete
    fetchDataForSelect("pannel/select/selectSocialMediaNames","socialmedia_delete_name","",createSelectFrom);
}

/**
 * this function is not used yet
 * 
 * send an ajax request to the server to upload an image
 * 
 * @param {string} form 
 * @param {string} script 
 */
function uploadImage(form,script)
{
    //console.log(form);
    //console.log(script);
    let request;
    //on ready 
    
    $(document).ready(()=>{
            //console.log($("#image_add_name").val());
            //let data = $("#image_add_name").val();
            let data = new FormData($("#delete_image_form")[0]);
            //console.log(data);
            // Abort any pending request
            if (request) {
                request.abort();
            }
            // Fire off the request to /form.php
            request = $.ajax({
                url: "/src/phpScripts/"+script,
                type: "post",
                processData: false,
                contentType: false,
                data: data
            });

            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR){
                // Log a message to the console
                //console.log(form+" updated");
                //console.log(response);
            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown){
                // Log the error to the console
                console.error(
                    "error :"+
                    textStatus, errorThrown
                );
            });
        });
}

function displayContacts(data,container){
    let tab = document.getElementById(container);
    data.forEach((item, index) => {
        let row = tab.insertRow(index);
        let col0 = row.insertCell(0);
        let col1 = row.insertCell(1);
        let col2 = row.insertCell(2);
        col0.innerHTML = item["name"];
        col1.innerHTML = item["email"];
        col2.innerHTML = item["message"];
    });
    
}

function fetchContacts(callback,script,container){
    let request;

    $(document).ready(()=>{
        // Abort any pending request
        if (request) {
            request.abort();
        }
        // Fire off the request to /form.php
        request = $.ajax({
            url: "/src/phpScripts/"+script,
            type: "post",
            dataType: "json"
        });

        // Callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR){
            // Log a message to the console
            callback(response,container);
        });

        // Callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown){
            // Log the error to the console
            console.error(
                "error :"+
                textStatus, errorThrown
            );
        });
    });
}


/**
 * sent an ajax request to the server to delete an image
 * 
 * @param {string} image 
 * @param {string} script 
 */
function deleteImage(image,script)
{
    //console.log($('select[name=\'' + image + '\']').val());
    //get the option value in the selector
    let data = $('select[name=\'' + image + '\']').val();
    let request = $.ajax({
        url: "/src/phpScripts/"+script,
        type: "post",
        data: {"data":data}
    });
    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        console.log(image+" deleted");
        $("#image_delete_name").empty();
        fetchDataForSelect("tools/selectImageNames","image_delete_name","",createImagesSelectFrom);
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "error :"+
            textStatus, errorThrown
        );
    });
}

//when the page is loaded, get all data from the db to fill all the form dynamically
getData("view/selectAllFromHomeSection",displayHomesectionFormData);
getData("view/selectAllFromProfil",displayProfilFormData);
getData("view/selectAllFromCompetences",displayCompetencesFormData);
getData("view/selectAllFromProjects",displayProjectFormData);
getData("view/selectAllFromSkills",displaySkillsFormData);
getData("view/selectAllFromSocialMedia",displaySocialMediaFormData);

//erase the image selector for deletion
$("#image_delete_name").empty();
//update the select
fetchDataForSelect("tools/selectImageNames","image_delete_name","",createImagesSelectFrom);
fetchContacts(displayContacts,"tools/selectContacts.php","contact_container");