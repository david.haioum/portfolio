/**
 * perform an ajax request on a form to a php script
 * 
 * @param {string} form_id form data
 * @param {string} php_script_name action script
 */
function execScript(form_id,php_script_name)
{
    //console.log(form_id)
    //console.log(php_script_name)
    let request;
    //on ready 
    
    $(document).ready(()=>{
            //console.log("start")
            // Abort any pending request
            if (request) {
                request.abort();
            }
            // setup some local variables
            var $form = $(form_id);

            // Let's select and cache all the fields
            var $inputs = $form.find("input, select, button, textarea");
            //console.log($inputs)
            // Serialize the data in the form
            var serializedData = $form.serializeArray();
            //console.log(serializedData);
            // Let's disable the inputs for the duration of the Ajax request.
            // Note: we disable elements AFTER the form data has been serialized.
            // Disabled form elements will not be serialized.
            $inputs.prop("disabled", true);

            // Fire off the request to /form.php
            request = $.ajax({
                url: "/src/phpScripts/"+php_script_name,
                type: "post",
                data: serializedData
            });

            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR){
                // Log a message to the console
                console.log(form_id+" updated");
                //console.log(response);
            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown){
                // Log the error to the console
                console.error(
                    "error :"+
                    textStatus, errorThrown
                );
            });

            // Callback handler that will be called regardless
            // if the request failed or succeeded
            request.always(function () {
                // Reenable the inputs
                $inputs.prop("disabled", false);
            });

        });
    
}