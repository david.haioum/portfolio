/*
file name : Sill.js

subject : this class add all the skills to the skillsSection division from the database

description : Skill class manage the display from the data served by the database



author : david haioum

input (exemple) : json 

[   
    {"id":"1",
        "0":"1",
        "logoLink":"img/circle.png",
        "1":"img/circle.png",
        "name":"SIMPLICITY",
        "2":"SIMPLICITY",
        "content":"\"Less is More\"",
        "3":"\"Less is More\""},
    {"id":"2",
        "0":"2",
        "logoLink":"img/pencil.png",
        "1":"img/pencil.png",
        "name":"CREATIVITY",
        "2":"CREATIVITY",
        "content":"Always think about \r\ncreative ideas",
        "3":"Always think about \r\ncreative ideas"}  
]

output (exemple) :

<div class="card noborder bg-blue">
    <div class="card-body text-center">
        <p class="card-text">
            <img class="icon48px" src="link" alt="alt">
        </p>
        <p class="card-text bold txt-yellow x-large-font">name</p>
        <p class="card-text bolder text-white x-large-font">content</p>
    </div>
</div>

*/

class Skill {
    constructor(json_data)
    {
        this.data = json_data
    }

    createSkill(container_id)
    {
        

    }
}