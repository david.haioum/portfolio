<?php   
    /**
     * manage skill table in a database
     */
    final class SkillsManager extends Manager {
        /**
         * add a skill in the database
         * 
         * note : when calling the function : use a try catch
         * to perform an error handling
         * 
         * @param string $logoLink 
         * @param string $name
         * @param string $content 
         * @return void
         */
        public function add($logoLink,$name,$content)
        {
            if(strlen($logoLink) <= 100)
            {
                //the length must be less than the varchar length in the db
                if(strlen($name) <= 30)
                {
                    if(strlen($content) <= 150 )
                    {
                        $request = "INSERT INTO `skills` 
                        (`id`, `logoLink`, `name`,`content`) 
                        VALUES (NULL, \"$logoLink\", \"$name\",\"$content\")";

                        /* exec the request or print the error message */
                        $this->_dbh->exec($request) or die(print_r($this->_dbh->errorInfo(), true)); 
                    }else{
                        //return a new error message
                        throw new Exception("error : the content is too large !");
                    }
                }else{
                    throw new Exception("error : the name is too large !");
                }
            }else{
                throw new Exception("error : the logo's link is too large !");
            }
        }
        
        /**
         * delete a skill in the database
         * 
         * @param string $name
         * @return void
         */
        public function deleteByName($name)
        {
            $requete = "DELETE FROM `skills` WHERE `skills`.`name` = \"$name\" ";
            $this->_dbh->exec($requete);
        }

        /**
         * delete the skill by it id
         * 
         * @param string $id
         * @return void
         */
        public function deleteById($id)
        {
            $requete = "DELETE FROM `skills` WHERE `skills`.`id` = '$id' ";
            $this->_dbh->exec($requete);
        }

        /**
         * select a skill in the database
         * 
         * @param string $name
         * @return Skill
         */
        public function selectByName($name)
        {
            $request = "SELECT * FROM skills WHERE name = \"$name\" ";
            $result = array();
            foreach($this->_dbh->query($request) as $raw){
               array_push($result,$raw);
            }
            return new Skill($result); 
        }

        /**
         * select the skill by it id
         * 
         * @param string $id
         * @return Skill
         */
        public function selectById($id)
        {
            $request = "SELECT * FROM skills WHERE id = '$id' ";
            $result = array();
            foreach($this->_dbh->query($request) as $raw){
               array_push($result,$raw);
            }
            return new Skill($result); 
        }
        
        /**
         * update a skill in the database from the object
         * 
         * @param Skill $skill
         * @return void
         */
        public function update(Skill $skill){
            $request="UPDATE skills 
            SET id = \"".$skill->getId()."\", 
            logoLink = \"".$skill->getLogoLink()."\", 
            name = \"".$skill->getName()."\",
            content = \"".$skill->getContent()."\"
            WHERE id = \"".$skill->getId()."\"";
            $this->_dbh->exec($request);
        }

        
    }
?>