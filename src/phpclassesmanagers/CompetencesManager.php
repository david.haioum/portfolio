<?php   
    /**
     * manage competences table in a database
     */
    final class CompetencesManager extends Manager {
        /**
         * add a competence in the 
         * 
         * note : when calling the function : use a try catch
         * to perform an error handling
         *
         * @param  string $name name of your competence
         * @param  string $percentage percentage of the competence [0-100] %
         *
         * @return void
         */
        public function add($name,$percentage)
        {
            //the length must be less than the varchar length in the db
            if(strlen($name) <= 30)
            {
                //Error handling on a pourcentage
                if($percentage > 0 && $percentage <= 100 )
                {
                    $request = "INSERT INTO `competences` (`id`, `name`, `percentage`) VALUES (NULL, \"$name\", '$percentage')";
                    /* exec the request or print the error message */
                    $this->_dbh->exec($request) or die(print_r($this->_dbh->errorInfo(), true));
                }else{
                    //return a new error message
                    throw new Exception("error : percentage must be between 0 and 100% !");
                }
            }else{
                throw new Exception("error : the name is too large !");
            }

        }
        
        /**
         * delete a competence in the database
         * 
         * @param string $name
         * @return void
         */
        public function deleteByName($name)
        {
            $requete = "DELETE FROM `competences` WHERE `competences`.`name` = \"$name\" ";
            $this->_dbh->exec($requete);
        }

        /**
         * delete the competence by it id
         * 
         * @param string $id
         * @return void
         */
        public function deleteById($id)
        {
            $requete = "DELETE FROM `competences` WHERE `competences`.`id` = '$id' ";
            $this->_dbh->exec($requete);
        }

        /**
         * select a competence in the database
         * 
         * @param string $name
         * @return Competence 
         */
        public function selectByName($name)
        {
            $request = "SELECT * FROM competences WHERE name = \"$name\" ";
            $result = array();
            foreach($this->_dbh->query($request) as $raw){
               array_push($result,$raw);
            }
            return new Competence($result); 
        }

        /**
         * select the competence by it id
         * 
         * @param string $id
         * @return Competence 
         */
        public function selectById($id)
        {
            $request = "SELECT * FROM competences WHERE id = '$id' ";
            $result = array();
            foreach($this->_dbh->query($request) as $raw){
               array_push($result,$raw);
            }
            return new Competence($result); 
        }
        
        /**
         * update a competence in the database from the object
         * 
         * @param Competence $competence
         * @return void
         */
        public function update(Competence $competence){
            $request="UPDATE competences 
            SET id = \"".$competence->getId()."\", 
            name = \"".$competence->getName()."\", 
            percentage = \"".$competence->getPercentage()."\"
            WHERE id = \"".$competence->getId()."\"";
            $this->_dbh->exec($request);
        }

        /**
         * select all table from $tableName order by percentage
         *
         * @param  string $tableName
         *
         * @return array
         */
        public function selectAllFromOrder($tableName)
        {
            $request = "SELECT * FROM $tableName ORDER BY percentage DESC";
            $result = array();
            foreach($this->_dbh->query($request) as $raw){
               array_push($result,$raw);
            }
            return $result; 
        }
    }
?>