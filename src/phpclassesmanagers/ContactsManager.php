<?php   
    /**
     * manage contacts table in a database
     */
    final class ContactsManager extends Manager {
        
        public function add($name,$email,$message)
        {
            //the length must be less than the varchar length in the db
            if(strlen($name) <= 100)
            {
                //Error handling on a pourcentage
                if(strlen($email) <= 100 )
                {
                    if( strlen($message) <= 500 ){
                        $request = "INSERT INTO `contact` (`id`, `name`, `email`, `message`) VALUES (NULL, \"$name\", '$email', '$message')";
                        /* exec the request or print the error message */
                        $this->_dbh->exec($request) or die(print_r($this->_dbh->errorInfo(), true));
                    }else{
                        throw new Exception("error : message too large !");
                    }
                }else{
                    //return a new error message
                    throw new Exception("error : email too large !");
                }
            }else{
                throw new Exception("error : the name is too large !");
            }

        }
        
        /**
         * delete a contact in the database
         * 
         * @param string $name
         * @return void
         */
        public function deleteByName($name)
        {
            $requete = "DELETE FROM `contact` WHERE `contact`.`name` = \"$name\" ";
            $this->_dbh->exec($requete);
        }

        /**
         * delete the contact by it id
         * 
         * @param string $id
         * @return void
         */
        public function deleteById($id)
        {
            $requete = "DELETE FROM `contact` WHERE `contact`.`id` = '$id' ";
            $this->_dbh->exec($requete);
        }

        /**
         * select a contact in the database
         * 
         * @param string $name
         * @return Contact 
         */
        public function selectByName($name)
        {
            $request = "SELECT * FROM contact WHERE name = \"$name\" ";
            $result = array();
            foreach($this->_dbh->query($request) as $raw){
               array_push($result,$raw);
            }
            return new Contact($result); 
        }

        /**
         * select the contact by it id
         * 
         * @param string $id
         * @return Contact 
         */
        public function selectById($id)
        {
            $request = "SELECT * FROM contact WHERE id = '$id' ";
            $result = array();
            foreach($this->_dbh->query($request) as $raw){
               array_push($result,$raw);
            }
            return new Contact($result); 
        }


        


    }
?>