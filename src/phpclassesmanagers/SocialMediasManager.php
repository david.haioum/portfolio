<?php   
    /**
     * manage socialmedia table in a database
     * 
     * /!\ just one line in the table
     */
    final class SocialMediasManager extends Manager {
        /**
         * add a socialmedia in the database
         * 
         * note : when calling the function : use a try catch
         * to perform an error handling
         * 
         * @param string $name name of your socialmedia
         * @param string $websiteLink website link
         * @param string $logoLink link to the logo (image)
         */
        public function add($name,$websiteLink,$logoLink)
        {
            if(strlen($logoLink) <= 200)
            {
                //the length must be less than the varchar length in the db
                if(strlen($name) <= 50)
                {
                    if(strlen($websiteLink) <= 200 )
                    {
                        $request = "INSERT INTO `socialmedia` 
                        (`id`, `name`, `websiteLink`,`logoLink`) 
                        VALUES (NULL, \"$name\", \"$websiteLink\",\"$logoLink\")";

                        /* exec the request or print the error message */
                        $this->_dbh->exec($request) or die(print_r($this->_dbh->errorInfo(), true)); 
                    }else{
                        //return a new error message
                        throw new Exception("error : the website link is too large !");
                    }
                }else{
                    throw new Exception("error : the name is too large !");
                }
            }else{
                throw new Exception("error : the logo link is too large !");
            }
        }

        /**
         * selectByName
         *
         * @param  string $name
         *
         * @return SocialMedia
         */
        public function selectByName($name)
        {
            $request = "SELECT * FROM socialmedia WHERE name = \"$name\" ";
            $result = array();
            foreach($this->_dbh->query($request) as $raw){
               array_push($result,$raw);
            }
            return new SocialMedia($result); 
        }

        /**
         * selectByName
         *
         * @param  string $id
         *
         * @return SocialMedia
         */
        public function selectById($id)
        {
            $request = "SELECT * FROM socialmedia WHERE id = \"$id\" ";
            $result = array();
            foreach($this->_dbh->query($request) as $raw){
               array_push($result,$raw);
            }
            return new SocialMedia($result); 
        }

        /**
         * deleteByName
         *
         * @param  string $name
         * @return void
         */
        public function deleteByName($name)
        {
            $requete = "DELETE FROM `socialmedia` WHERE `socialmedia`.`name` = \"$name\" ";
            $this->_dbh->exec($requete);
        }

        /**
         * deleteById
         *
         * @param  string $id
         * @return void
         */
        public function deleteById($id)
        {
            $requete = "DELETE FROM `socialmedia` WHERE `socialmedia`.`id` = \"$id\" ";
            $this->_dbh->exec($requete);
        }
        
        /**
         * update a socialmedia in the database from the object
         * 
         * @param SocialMedia $socialmedia
         * @return void
         */
        public function update(SocialMedia $socialmedia){
            $request="UPDATE socialmedia 
            SET name = \"".$socialmedia->getName()."\",
            websiteLink = \"".$socialmedia->getWebsiteLink()."\",
            logoLink = \"".$socialmedia->getLogoLink()."\"".
            " WHERE id=\"".$socialmedia->getId()."\"";
            echo $request;
            $this->_dbh->exec($request);
        }       

        
    }
?>