<?php   
    /**
     * manage profil table in a database
     * 
     * /!\ just one line in the table
     */
    final class ProfilManager extends Manager {

        public function selectByName($name){
            
        }
        public function selectById($id){

        }
        public function deleteByName($name){

        }
        public function deleteById($id){

        }

        /**
         * select the profil's content 
         * 
         * @return Profil
         */
        public function select()
        {
            $request = "SELECT * FROM profil";
            $result = array();
            foreach($this->_dbh->query($request) as $raw){
               array_push($result,$raw);
            }
            return new Profil($result); 
        }
        
        /**
         * update a profil in the database from the object
         * 
         * @param Profil $profil
         * @return void
         */
        public function update(Profil $profil){
            $request="UPDATE profil 
            SET id = \"".$profil->getId()."\", 
            profession = \"".$profil->getProfession()."\",
            place = \"".$profil->getPlace()."\",
            placeLink = \"".$profil->getPlaceLink()."\",
            companyName = \"".$profil->getCompanyName()."\",
            companyLink = \"".$profil->getCompanyLink()."\",
            faceImgLink = \"".$profil->getFaceImgLink()."\",
            hobbies = \"".$profil->getHobbies()."\"
            WHERE id = \"".$profil->getId()."\"";
            echo $request;
            $this->_dbh->exec($request);
        }        

        
    }
?>