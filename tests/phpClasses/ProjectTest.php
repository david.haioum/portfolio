<?php 

const project_data = Array ( 0 => Array ( 
    "id" => "1" ,
    "name" => "Lorem",
    "description" => "Ipsem",
    "imageLink1" => "foo",
    "imageLink2" => "bar",
    "technoName" => "link1"
) );

use PHPUnit\Framework\TestCase;

final class ProjectTest extends TestCase {

    public function test_Project_construct()
    {
        $project = new Project(project_data);
        
        $this->assertInstanceOf(Project::class,$project);
    }
    public function test_Project_getters()
    {
        $project = new Project(project_data);

        $this->assertEquals($project->getId(),"1");
        $this->assertEquals($project->getName(),"Lorem");
        $this->assertEquals($project->getDescription(),"Ipsem");
        $this->assertEquals($project->getImageLink1(),"foo");
        $this->assertEquals($project->getImageLink2(),"bar");
        $this->assertEquals($project->getTechnoName(),"link1");
    }
    public function test_Project_setters()
    {
        $project = new Project(project_data);

        $project->setId("2");
        $project->setName("LOS ANGELES");
        $project->setDescription("wikipedia");
        $project->setImageLink1("driver");
        $project->setImageLink2("tesco");
        $project->setTechnoName("uk.co");

        $this->assertEquals($project->getId(),"2");
        $this->assertEquals($project->getName(),"LOS ANGELES");
        $this->assertEquals($project->getDescription(),"wikipedia");
        $this->assertEquals($project->getImageLink1(),"driver");
        $this->assertEquals($project->getImageLink2(),"tesco");
        $this->assertEquals($project->getTechnoName(),"uk.co");
    }
}


?>