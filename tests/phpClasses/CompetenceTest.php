<?php 

const competence_data = Array ( 0 => Array ( 
    "id" => "1" ,
    "name" => "foo" ,
    "percentage" => "88" 
) );

use PHPUnit\Framework\TestCase;

final class CompetenceTest extends TestCase {

    public function test_Competence_construct()
    {
        $competence = new Competence(competence_data);
        
        $this->assertInstanceOf(Competence::class,$competence);
    }
    public function test_Competence_getters()
    {
        $competence = new Competence(competence_data);

        $this->assertEquals($competence->getId(),"1");
        $this->assertEquals($competence->getName(),"foo");
        $this->assertEquals($competence->getPercentage(),"88");
    }
    public function test_Competence_setters()
    {
        $competence = new Competence(competence_data);

        $competence->setId("2");
        $competence->setName("bar");
        $competence->setPercentage("23");

        $this->assertEquals($competence->getId(),"2");
        $this->assertEquals($competence->getName(),"bar");
        $this->assertEquals($competence->getPercentage(),"23");
    }
}


?>