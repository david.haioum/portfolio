<?php 

const skill_data = Array ( 0 => Array ( 
    "id" => "1" ,
    "logoLink" => "Lorem",
    "name" => "Ipsem",
    "content" => "foo"
) );

use PHPUnit\Framework\TestCase;

final class SkillTest extends TestCase {

    public function test_Skill_construct()
    {
        $skill = new Skill(skill_data);
        
        $this->assertInstanceOf(Skill::class,$skill);
    }
    public function test_Skill_getters()
    {
        $skill = new Skill(skill_data);

        $this->assertEquals($skill->getId(),"1");
        $this->assertEquals($skill->getLogoLink(),"Lorem");
        $this->assertEquals($skill->getName(),"Ipsem");
        $this->assertEquals($skill->getContent(),"foo");
    }
    public function test_Skill_setters()
    {
        $skill = new Skill(skill_data);

        $skill->setId("2");
        $skill->setLogoLink("LOS ANGELES");
        $skill->setName("wikipedia");
        $skill->setContent("driver");

        $this->assertEquals($skill->getId(),"2");
        $this->assertEquals($skill->getLogoLink(),"LOS ANGELES");
        $this->assertEquals($skill->getName(),"wikipedia");
        $this->assertEquals($skill->getContent(),"driver");
    }
}


?>