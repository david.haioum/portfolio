<?php 

const homesection_data = Array ( 0 => Array ( 
    "id" => "1" ,
    "name" => "foo bar" ,
    "profession" => "profossion's name" ,
    "companyName" => "company's name" ,
    "companyWebSite" => "https://foo.fr" ,
    "email" => "foo@bar.com"
) );

use PHPUnit\Framework\TestCase;

final class HomesectionTest extends TestCase {

    public function test_Homesection_construct()
    {
        $homesection = new HomeSection(homesection_data);
        
        $this->assertInstanceOf(HomeSection::class,$homesection);
    }
    public function test_Homesection_getters()
    {
        $homesection = new HomeSection(homesection_data);

        $this->assertEquals($homesection->getId(),"1");
        $this->assertEquals($homesection->getName(),"foo bar");
        $this->assertEquals($homesection->getProfession(),"profossion's name");
        $this->assertEquals($homesection->getCompanyName(),"company's name");
        $this->assertEquals($homesection->getCompanyWebSite(),"https://foo.fr");
        $this->assertEquals($homesection->getEmail(),"foo@bar.com");
    }
    public function test_Homesection_setters()
    {
        $homesection = new HomeSection(homesection_data);

        $homesection->setId("2");
        $homesection->setName("Lorem");
        $homesection->setProfession("Ipsem");
        $homesection->setCompanyName("Eta");
        $homesection->setCompanyWebSite("https://bar.fr");
        $homesection->setEmail("bar@foo.com");

        $this->assertEquals($homesection->getId(),"2");
        $this->assertEquals($homesection->getName(),"Lorem");
        $this->assertEquals($homesection->getProfession(),"Ipsem");
        $this->assertEquals($homesection->getCompanyName(),"Eta");
        $this->assertEquals($homesection->getCompanyWebSite(),"https://bar.fr");
        $this->assertEquals($homesection->getEmail(),"bar@foo.com");
    }
}


?>