<?php 

const profil_data = Array ( 0 => Array ( 
    "id" => "1" ,
    "place" => "Lorem",
    "placeLink" => "Ipsem",
    "profession" => "foo",
    "companyName" => "bar",
    "companyLink" => "link1",
    "faceImgLink" => "link2",
    "hobbies" => "This is a funny test"
) );

use PHPUnit\Framework\TestCase;

final class ProfilTest extends TestCase {

    public function test_Profil_construct()
    {
        $profil = new Profil(profil_data);
        
        $this->assertInstanceOf(Profil::class,$profil);
    }
    public function test_Profil_getters()
    {
        $profil = new Profil(profil_data);

        $this->assertEquals($profil->getId(),"1");
        $this->assertEquals($profil->getPlace(),"Lorem");
        $this->assertEquals($profil->getPlaceLink(),"Ipsem");
        $this->assertEquals($profil->getProfession(),"foo");
        $this->assertEquals($profil->getCompanyName(),"bar");
        $this->assertEquals($profil->getCompanyLink(),"link1");
        $this->assertEquals($profil->getFaceImgLink(),"link2");
        $this->assertEquals($profil->getHobbies(),"This is a funny test");
    }
    public function test_Profil_setters()
    {
        $profil = new Profil(profil_data);

        $profil->setId("2");
        $profil->setPlace("LOS ANGELES");
        $profil->setPlaceLink("wikipedia");
        $profil->setProfession("driver");
        $profil->setCompanyName("tesco");
        $profil->setCompanyLink("uk.co");
        $profil->setFaceImgLink("img/face.png");
        $profil->setHobbies("I'm happy to do this test");

        $this->assertEquals($profil->getId(),"2");
        $this->assertEquals($profil->getPlace(),"LOS ANGELES");
        $this->assertEquals($profil->getPlaceLink(),"wikipedia");
        $this->assertEquals($profil->getProfession(),"driver");
        $this->assertEquals($profil->getCompanyName(),"tesco");
        $this->assertEquals($profil->getCompanyLink(),"uk.co");
        $this->assertEquals($profil->getFaceImgLink(),"img/face.png");
        $this->assertEquals($profil->getHobbies(),"I'm happy to do this test");
    }
}


?>