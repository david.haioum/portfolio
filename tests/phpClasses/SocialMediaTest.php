<?php 

const socialmedia_data = Array ( 0 => Array ( 
    "id" => "1" ,
    "Name" => "Lorem",
    "WebsiteLink" => "Ipsem",
    "LogoLink" => "foo"
) );

use PHPUnit\Framework\TestCase;

final class SocialMediaTest extends TestCase {

    public function test_SocialMedia_construct()
    {
        $socialmedia = new SocialMedia(socialmedia_data);
        
        $this->assertInstanceOf(SocialMedia::class,$socialmedia);
    }
    public function test_SocialMedia_getters()
    {
        $socialmedia = new SocialMedia(socialmedia_data);

        $this->assertEquals($socialmedia->getId(),"1");
        $this->assertEquals($socialmedia->getName(),"Lorem");
        $this->assertEquals($socialmedia->getWebsiteLink(),"Ipsem");
        $this->assertEquals($socialmedia->getLogoLink(),"foo");
    }
    public function test_SocialMedia_setters()
    {
        $socialmedia = new SocialMedia(socialmedia_data);

        $socialmedia->setId("2");
        $socialmedia->setName("LOS ANGELES");
        $socialmedia->setWebsiteLink("wikipedia");
        $socialmedia->setLogoLink("driver");

        $this->assertEquals($socialmedia->getId(),"2");
        $this->assertEquals($socialmedia->getName(),"LOS ANGELES");
        $this->assertEquals($socialmedia->getWebsiteLink(),"wikipedia");
        $this->assertEquals($socialmedia->getLogoLink(),"driver");
    }
}


?>