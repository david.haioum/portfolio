<?php 

const contact_data = Array ( 0 => Array ( 
    "id" => "1" ,
    "name" => "bernard" ,
    "email" => "foo@bar.com",
    "message" => "Lorem Ipsum"
) );

use PHPUnit\Framework\TestCase;

final class ContactTest extends TestCase {

    public function test_contact_construct()
    {
        $contact = new Contact(contact_data);
        
        $this->assertInstanceOf(Contact::class,$contact);
    }
    public function test_contact_getters()
    {
        $contact = new Contact(contact_data);

        $this->assertEquals($contact->getId(),"1");
        $this->assertEquals($contact->getName(),"bernard");
        $this->assertEquals($contact->getEmail(),"foo@bar.com");
        $this->assertEquals($contact->getMessage(),"Lorem Ipsum");
    }
    public function test_contact_setters()
    {
        $contact = new Contact(contact_data);

        $contact->setId("2");
        $contact->setName("bar");
        $contact->setEmail("bar@foo.com");
        $contact->setMessage("azertyuiop");

        $this->assertEquals($contact->getId(),"2");
        $this->assertEquals($contact->getName(),"bar");
        $this->assertEquals($contact->getEmail(),"bar@foo.com");
        $this->assertEquals($contact->getMessage(),"azertyuiop");
    }
}


?>