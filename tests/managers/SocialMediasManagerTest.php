<?php 

//include classes
require "../../src/phpClassesManagers/Manager.php";
require "../../src/phpClassesManagers/SocialMediasManager.php";
require "../../src/phpClasses/SocialMedia.php";

//connection to the db
$socialMediasMan = new SocialMediasManager("localhost","portfolio","root","");
$socialMediasMan->connect();

//SELECT ALL TEST

// /!\ we need utf8_encode() when we will display the data on the website

echo "<p>display all data from db :</p>";
$all = $socialMediasMan->selectAllFrom('socialMedia');
print_r($all);

//SELECT ALL NAMES TEST
echo "<p>display all names from db :</p>";
$all = $socialMediasMan->selectAllNamesFrom('socialmedia');
print_r($all);

//SELECT BY ID TEST
echo "<p>display data by id from db :</p>";
$p = $socialMediasMan->selectById(1);
print_r($p);

//SELECT BY NAME TEST
echo "<p>display a data by name from db :</p>";
$p = $socialMediasMan->selectByName("gitlab");
print_r($p);

//ADD TEST
echo "<p>add a new project :</p>";
$socialMediasMan->add("bar","http://foo.com","img/gitlab.png");
$p = $socialMediasMan->selectByName("bar");
print_r($p);

//UPDATE TEST
echo "<p>update 'bar' project localy :</p>";
$p->setName("foo");
print_r($p);

echo "<p>update 'bar' project on db :</p>";
$socialMediasMan->update($p);
$p = $socialMediasMan->selectByName("foo");
print_r($p);

//DELETE BY NAME
echo "<p>delete :</p>";
$socialMediasMan->deleteByName("foo");
$all = $socialMediasMan->selectAllFrom('socialMedia');
print_r($all);

?>