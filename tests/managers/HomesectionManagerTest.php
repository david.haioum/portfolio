<?php 

/**
 * Unit tests with a database are too complex to implements with phpunit
 * So this script just test the HomesectionManager classe to verify especially 
 * the syntax of the sql commands
 * 
 * for the managers only , I wrote some simple script to perform unit tests
 */

//include classes
require "../../src/phpClassesManagers/Manager.php";
require "../../src/phpClassesManagers/HomesectionManager.php";
require "../../src/phpClasses/Homesection.php";

//connection to the db
$homesectionMan = new HomesectionManager("localhost","portfolio","root","");
$homesectionMan->connect();

//SELECTION TEST
echo "<p>display data from db :</p>";
$homesection = $homesectionMan->select();
print_r($homesection);

//MODIFY THE NAME
$homesection->setName("David");

//UPDATE TEST
$homesectionMan->update($homesection);

//VERIFICATION
echo "<p>verify update :</p>";
$homesection = $homesectionMan->select();
print_r($homesection);
echo "<br><br>";

//CLEAN THE TEST
echo "<p>verify cleaning :</p>";
$homesection->setName("David Haioum");
$homesectionMan->update($homesection);
//VERIFY CLEANING
$homesection = $homesectionMan->select();
print_r($homesection);

?>