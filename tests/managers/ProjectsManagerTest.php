<?php 

//include classes
require "../../src/phpClassesManagers/Manager.php";
require "../../src/phpClassesManagers/ProjectsManager.php";
require "../../src/phpClasses/Project.php";

//connection to the db
$projectsMan = new ProjectsManager("localhost","portfolio","root","");
$projectsMan->connect();

//SELECT ALL TEST

// /!\ we need utf8_encode() when we will display the data on the website

echo "<p>display all data from db :</p>";
$all = $projectsMan->selectAllFrom('projects');
print_r($all);

//SELECT ALL NAMES TEST
echo "<p>display all names from db :</p>";
$all = $projectsMan->selectAllNamesFrom('projects');
print_r($all);

//SELECT BY ID TEST
echo "<p>display data by id from db :</p>";
$p = $projectsMan->selectById(1);
print_r($p);

//SELECT BY NAME TEST
echo "<p>display a data by name from db :</p>";
$p = $projectsMan->selectByName("OMICRON SGBD");
print_r($p);

//ADD TEST
echo "<p>add a new project :</p>";
$projectsMan->add("foo","Lorem Ipsum","PHP","img/github.png","img/gitlab.png");
$p = $projectsMan->selectByName("foo");
print_r($p);

//UPDATE TEST
echo "<p>update 'foo' project localy :</p>";
$p->setName("GAME");
print_r($p);

echo "<p>update 'foo' project on db :</p>";
$projectsMan->update($p);
$p = $projectsMan->selectByName("GAME");
print_r($p);

//DELETE BY NAME
echo "<p>delete :</p>";
$projectsMan->deleteByName("GAME");
$all = $projectsMan->selectAllFrom('projects');
print_r($all);

?>