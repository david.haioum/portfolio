<?php 

/**
 * Unit tests with a database are too complex to implements with phpunit
 * So this script just test the profilManager classe to verify especially 
 * the syntax of the sql commands
 * 
 * for the managers only , I wrote some simple script to perform unit tests
 */

//include classes
require "../../src/phpClassesManagers/Manager.php";
require "../../src/phpClassesManagers/ProfilManager.php";
require "../../src/phpClasses/Profil.php";

//connection to the db
$profilMan = new ProfilManager("localhost","portfolio","root","");
$profilMan->connect();

//SELECTION TEST
echo "<p>display data from db :</p>";
$profil = $profilMan->select();
print_r($profil);

//MODIFY 
echo "<p>modify the object :</p>";
$profil->setProfession("developer");
print_r($profil);

//UPDATE 
$profilMan->update($profil);

//VERIFICATION
echo "<p>verify update :</p>";
$profil = $profilMan->select();
print_r($profil);
echo "<br><br>";

//CLEAN THE TEST
echo "<p>verify cleaning :</p>";
$profil->setProfession("student");
$profilMan->update($profil);
//VERIFY CLEANING
$profil = $profilMan->select();
print_r($profil);

?>