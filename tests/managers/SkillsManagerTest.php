<?php 

//include classes
require "../../src/phpClassesManagers/Manager.php";
require "../../src/phpClassesManagers/SkillsManager.php";
require "../../src/phpClasses/Skill.php";

//connection to the db
$skillsMan = new SkillsManager("localhost","portfolio","root","");
$skillsMan->connect();

//SELECT ALL TEST

// /!\ we need utf8_encode() when we will display the data on the website

echo "<p>display all data from db :</p>";
$all = $skillsMan->selectAllFrom('skills');
print_r($all);

//SELECT ALL NAMES TEST
echo "<p>display all names from db :</p>";
$all = $skillsMan->selectAllNamesFrom('skills');
print_r($all);

//SELECT BY ID TEST
echo "<p>display a data by id from db :</p>";
$s = $skillsMan->selectById(1);
print_r($s);

//SELECT BY NAME TEST
echo "<p>display a data by name from db :</p>";
$s = $skillsMan->selectByName("REFLECTION");
print_r($s);

//ADD TEST
echo "<p>add a new project :</p>";
$skillsMan->add("img/foo.png","bar","Lorem Ipsum");
$s = $skillsMan->selectByName("bar");
print_r($s);

//UPDATE TEST
echo "<p>update 'bar' project localy :</p>";
$s->setName("BAR");
print_r($s);

echo "<p>update 'bar' project on db :</p>";
$skillsMan->update($s);
$s = $skillsMan->selectByName("BAR");
print_r($s);

//DELETE BY NAME
echo "<p>delete :</p>";
$skillsMan->deleteByName("BAR");
$all = $skillsMan->selectAllFrom('skills');
print_r($all);

?>