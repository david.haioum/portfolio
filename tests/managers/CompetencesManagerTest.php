<?php 

//include classes
require "../../src/phpClassesManagers/Manager.php";
require "../../src/phpClassesManagers/CompetencesManager.php";
require "../../src/phpClasses/Competence.php";

//connection to the db
$competencesMan = new CompetencesManager("localhost","portfolio","root","");
$competencesMan->connect();

//SELECT ALL TEST
echo "<p>display all data from db :</p>";
$all = $competencesMan->selectAllFrom('competences');
print_r($all);

//SELECT ALL NAMES TEST
echo "<p>display all names from db :</p>";
$all = $competencesMan->selectAllNamesFrom('competences');
print_r($all);

//SELECT BY ID TEST
echo "<p>display a data by id from db :</p>";
$c = $competencesMan->selectById(1);
print_r($c);

//SELECT BY NAME TEST
echo "<p>display a data by name from db :</p>";
$c = $competencesMan->selectByName("HTML");
print_r($c);

//ADD TEST
echo "<p>add a new competence :</p>";
$competencesMan->add("DANSE",88);
$c = $competencesMan->selectByName("DANSE");
print_r($c);

//UPDATE TEST
echo "<p>update 'DANSE' competence :</p>";
$c->setName("GAMING");
$c->setPercentage("90");
$competencesMan->update($c);
$c = $competencesMan->selectByName("GAMING");
print_r($c);

//DELETE BY NAME
echo "<p>delete :</p>";
$competencesMan->deleteByName("GAMING");
$all = $competencesMan->selectAllFrom('competences');
print_r($all);

?>