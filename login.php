<?php 

$err = 0;

if( isset($_GET["error"]) ){
    $err = $_GET["error"];
}else{
    $err = 0;
}

require_once './vendor/autoload.php';

$host = "localhost";
$db = "portfolio";
$login = "root";
$pass = "";

//template render
$loader = new Twig_Loader_Filesystem(__DIR__.'/templates');
//environnement with options

/**

don't forget to renable the cache to optimize performances
and throw all content in tmp/ before disabling

*/

//when developing, disable the cache system
//don't forget to enable it at the end
$twig = new Twig_Environment($loader, [
    'cache' => false,//__DIR__.'/tmp',
    'debug' => true //to use dump()
]);

//debug extension
$twig->addExtension(new Twig_Extension_Debug());

echo $twig->render('loginModel.html', [
    "error" => $err
]);










?>