<?php   
    /**
     * manage homesection table in a database
     * 
     * /!\ just one line in the table
     */
    final class HomesectionManager extends Manager {

        public function selectByName($name){
            
        }
        public function selectById($id){

        }
        public function deleteByName($name){

        }
        public function deleteById($id){

        }

        /**
         * select the homesection's content 
         * 
         * @return HomeSection 
         */
        public function select()
        {
            $request = "SELECT * FROM homesection";
            $result = array();
            foreach($this->_dbh->query($request) as $raw){
               array_push($result,$raw);
            }
            return new HomeSection($result); 
        }
        
        /**
         * update a homesection in the database from the object
         * 
         * @param HomeSection $homesection
         * @return void
         */
        public function update(HomeSection $homesection){
            $request="UPDATE homesection 
            SET id = \"".$homesection->getId()."\", 
            name = \"".$homesection->getName()."\", 
            profession = \"".$homesection->getProfession()."\",
            companyName = \"".$homesection->getCompanyName()."\",
            companyWebSite = \"".$homesection->getCompanyWebSite()."\",
            email = \"".$homesection->getEmail()."\"
            WHERE id = \"".$homesection->getId()."\"";
            $this->_dbh->exec($request);
        }

        
    }
?>
