<?php   
    /**
     * manage projects table in a database
     */
    final class ProjectsManager extends Manager {
        /**
         * add a project in the database
         * 
         * note : when calling the function : use a try catch
         * to perform an error handling
         * 
         * @param string $name name of your project
         * @param string $description description 
         * @param string $technoName main programming langage name 
         * @param string $imgLink1 link of the main image 
         * @param string $imgLink2 link of the view image 
         * @return void
         */
        public function add($name,$description,$technoName,$imgLink1,$imgLink2)
        {
            //the length must be less than the varchar length in the db
            if(strlen($name) <= 100)
            {
                //Error handling
                if($description <= 400 )
                {
                    
                    if( strlen($imgLink1) <= 200)
                    {
                        if( strlen($imgLink2) <= 200)
                        {
                            $request = "INSERT INTO `projects` 
                            (`id`, `name`, `description`,`technoName`,`imageLink1`,`imageLink2`) 
                            VALUES (NULL, \"$name\", \"$description\",\"$technoName\",\"$imgLink1\",\"$imgLink2\")";

                            /* exec the request or print the error message */
                            $this->_dbh->exec($request) or die(print_r($this->_dbh->errorInfo(), true));
                        }else{
                            throw new Exception("error : 2nd image path too large !");
                        }
                    }else{
                        throw new Exception("error : 1st image path too large !");
                    }
                }else{
                    //return a new error message
                    throw new Exception("error : the description is too large !");
                }
            }else{
                throw new Exception("error : the name is too large !");
            }

        }
        
        /**
         * delete a project in the database
         * 
         * @param string $name
         */
        public function deleteByName($name)
        {
            $requete = "DELETE FROM `projects` WHERE `projects`.`name` = \"$name\" ";
            $this->_dbh->exec($requete);
        }

        /**
         * delete the project by it id
         * 
         * @param string $id
         * @return void
         */
        public function deleteById($id)
        {
            $requete = "DELETE FROM `projects` WHERE `projects`.`id` = '$id' ";
            $this->_dbh->exec($requete);
        }

        /**
         * select a project in the database
         * 
         * @param string $name
         * @return Project 
         */
        public function selectByName($name)
        {
            $request = "SELECT * FROM projects WHERE name = \"$name\" ";
            $result = array();
            foreach($this->_dbh->query($request) as $raw){
               array_push($result,$raw);
            }
            return new Project($result); 
        }

        /**
         * select the project by it id
         * 
         * @param string $id
         * @return Project 
         */
        public function selectById($id)
        {
            $request = "SELECT * FROM projects WHERE id = '$id' ";
            $result = array();
            foreach($this->_dbh->query($request) as $raw){
               array_push($result,$raw);
            }
            return new Project($result); 
        }
        
        /**
         * update a project in the database from the object
         * 
         * @param Project $project
         */
        public function update(Project $project){
            $request="UPDATE projects 
            SET id = \"".$project->getId()."\", 
            name = \"".$project->getname()."\", 
            description = \"".$project->getDescription()."\",
            technoName = \"".$project->getTechnoName()."\",
            imageLink1 = \"".$project->getImageLink1()."\",
            imageLink2 = \"".$project->getImageLink2()."\"
            WHERE id = \"".$project->getId()."\"";
            $this->_dbh->exec($request);
        }
        
    }
?>
