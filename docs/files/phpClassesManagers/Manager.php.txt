<?php   
    /**
     * manage a table in a database
     */
    abstract class Manager {
        protected $_host; 
        protected $_db; 
        protected $_user; 
        protected $_pass; 
        protected $_dbh;

        /**
         * set all the attribute
         * @param string $host name of the host for the database
         * @param string $db name of the database
         * @param string $user username
         * @param string $pass password
         * @return void
         */
        public function __construct($host,$db,$user,$pass)
        {
            $this->_host = $host;
            $this->_db = $db;
            $this->_user = $user;
            $this->_pass = $pass;
        }

        /**
         * connection to the data base by using the PDO object
         * 
         * @return void
         */
        public function connect()
        {
            try {
                $this->_dbh = new PDO('mysql:host='.$this->_host.';dbname='.$this->_db,$this->_user,$this->_pass);
            }
            catch(PDOException $e)
            {
                echo "Connection failed: " . $e->getMessage();
            }
        }
        
        /**
         * select all table from $tableName
         *
         * @param  string $tableName
         *
         * @return array
         */
        public function selectAllFrom($tableName)
        {
            $request = "SELECT * FROM $tableName";
            $result = array();
            foreach($this->_dbh->query($request) as $raw){
               array_push($result,$raw);
            }
            return $result; 
        }

        /**
         * select all names from $tableName
         *
         * @param  string $tableName
         *
         * @return array
         */
        public function selectAllNamesFrom($tableName)
        {
            $request = "SELECT name FROM $tableName";
            $result = array();
            foreach($this->_dbh->query($request) as $raw){
               array_push($result,$raw);
            }
            return $result; 
        }

        /**
         * and for each classe :
         * 
         * -add
         * -update
         * 
         */

        abstract public function deleteByName($name);
        abstract public function deleteById($id);

        abstract public function selectByName($name);
        abstract public function selectById($id);
    }
?>
