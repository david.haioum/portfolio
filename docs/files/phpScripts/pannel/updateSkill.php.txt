<?php 

/*
$_POST["skill_name"]
$_POST["skill_newName"]
$_POST["skill_newLogoLink"]
$_POST["skill_content"]
*/

require "../../phpClasses/Skill.php";
require "../../phpClassesManagers/Manager.php";
require "../../phpClassesManagers/SkillsManager.php";

$manager = new SkillsManager("localhost","portfolio","root","");
$manager->connect();

if(isset($_POST["skill_name"])){
    $hm = $manager->selectByName($_POST["skill_name"]);


    if(isset($_POST["skill_newName"])){
        $hm->setName($_POST["skill_newName"]);
    }
    if(isset($_POST["skill_newLogoLink"])){
        $hm->setLogoLink($_POST["skill_newLogoLink"]);
    }
    if(isset($_POST["skill_content"])){
        $hm->setContent($_POST["skill_content"]);
    }
    $manager->update($hm);
}




?>
